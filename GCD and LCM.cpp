#include <iostream>
using namespace std;

int gcd (long a, long b)
{
    if (a % b == 0) return b;
    
    return gcd (b, a % b);
    
    // while (b)
    // {
    //     a %= b;
    //     swap (a , b);
    // }
    // return a;
}

int main() {
    int t;
    cin>>t;
    
    while (t--)
    {
        long a;
        long b;
        
        cin>>a>>b;
        
        cout << gcd (a,b) << " " << a * b /gcd (a,b)<<endl;
    }
}
    