#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
	    int p;
	    cin >>p;
	    
	    int menuItems = 0;
	    for(int i=11; i>=0;i--) {
	        int currPower = pow(2,i);
	        while(p >= currPower) {
	            p-=currPower;
	            menuItems++;
	        }
	    }
	    
	    cout << menuItems << endl;
	}
	return 0;
}
