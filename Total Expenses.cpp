#include <iostream>
#include <bits/stdc++.h>
using namespace std;

int main() {
    int t;
    cin >> t;
    while (t--)
    {
        int q,p;
        cin >> q >> p;
        double totalprice = q * p;
        if(q>1000) {
            totalprice -= totalprice * 0.1;
        }

        cout << setprecision(10) << fixed;
        cout << totalprice << endl;

    }
    return 0;
    
}