#include <iostream>
using namespace std;

int fno(int n){
         
         while(n/10>0){
                  
                  n=n/10;
                  
         }
         return n;
}

int lno(int n){

    int s=n%10;
     return s;
}

int main() {
	// your code goes here
	int T;
	cin>>T;
	while(T--){
	         int n;
	         cin>>n;
	        cout<<fno(n)+lno(n)<<endl;
	}
	return 0;
}